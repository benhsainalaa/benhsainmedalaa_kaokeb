function openMenu() {
    
    document.getElementById("open").classList.add("hide");
    document.getElementById("close").classList.add("show");
    document.getElementById("close").classList.remove("hide");
    document.getElementById("navbar-menu").classList.add("navbar-menu-mobile");
    document.getElementById("navbar-menu").classList.remove("navbar-menu");
    document.getElementById("navbar-menu-item").classList.add("navbar-menu-item-mobile");
    document.getElementById("navbar-menu-item").classList.remove("navbar-menu-item");
    document.getElementById("content").classList.add("content-mobile");
    document.getElementById("container-2-title").classList.add("container-2-title-mobile");
    document.getElementById("container-2").classList.add("container-2-mobile");
    document.getElementById("container-3").classList.add("container-3-mobile");
    document.getElementById("container-4").classList.add("container-4-mobile");
    document.getElementById("container-5").classList.add("container-5-mobile");
    document.getElementById("container-6").classList.add("container-6-mobile"); 
    
  }

  function closeMenu() {
    
    
    document.getElementById("content").classList.remove("content-mobile");
    document.getElementById("container-2-title").classList.remove("container-2-title-mobile");
    document.getElementById("container-2").classList.remove("container-2-mobile");
    document.getElementById("container-3").classList.remove("container-3-mobile");
    document.getElementById("container-4").classList.remove("container-4-mobile");
    document.getElementById("container-5").classList.remove("container-5-mobile");
    document.getElementById("container-6").classList.remove("container-6-mobile");
    document.getElementById("open").classList.add("show");
    document.getElementById("open").classList.remove("hide");
    document.getElementById("close").classList.add("hide");

    document.getElementById("navbar-menu").classList.add("navbar-menu");
    document.getElementById("navbar-menu").classList.remove("navbar-menu-mobile");
    document.getElementById("navbar-menu-item").classList.add("navbar-menu-item");
    document.getElementById("navbar-menu-item").classList.remove("navbar-menu-item-mobile");
    
  }

  function toDesktop(x) {
    if (!x.matches) 
       closeMenu();
  }

  var x = window.matchMedia("(max-width: 700px)")
  x.addListener(toDesktop);
  
    
  
  
  